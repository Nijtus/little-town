package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;



public class App extends FlowPane implements Initializable {
    @FXML
    public Pane pane2;
    @FXML
    public Pane pane1;
    @FXML
    public Pane rootPane;
    @FXML
    public Pane choose;
    @FXML
    public Pane choose2;
    @FXML
    public Label labelJ1;
    @FXML
    public  Label joueur ;
    @FXML
    public ImageView ouvrier;
    @FXML
    public ImageView etoileJ;

    @FXML
    public ImageView tour1;
    @FXML
    public ImageView tour2;
    @FXML
    public ImageView tour3;
    @FXML
    public ImageView tour4;
    @FXML
    public ImageView case0;
    @FXML
    public ImageView case3;
    @FXML
    public Button changetour;
    @FXML
    public Button test;
    @FXML
    public Button gagnerPoint;
    @FXML
    public Button changeJoueur;
    @FXML
    public Label objectifJ1;
    @FXML
    public Label objectifJ2;
    @FXML
    public CheckBox check2;
    @FXML
    public CheckBox check3;
    @FXML
    public CheckBox check4;
    @FXML
    public Button valid;
    @FXML
    public Button marche30;
    @FXML
    public Button marche40;
    @FXML
    public Button marche50;
    @FXML
    public Button marche60;
    @FXML
    public Button marche70;
    @FXML
    public Button marche80;
    @FXML
    public Button marche31;
    @FXML
    public Button marche41;
    @FXML
    public Button marche51;
    @FXML
    public Button marche61;
    @FXML
    public Button marche71;
    @FXML
    public Button marche81;
    @FXML
    public Label lab1;
    @FXML
    public Label pointObjectif;
    @FXML
    public Label victoire;



    @FXML
    public GridPane pointsHaut;
    @FXML
    public GridPane pointsBas;
    @FXML
    public GridPane pointsGauche;
    @FXML
    public GridPane pointsDroit;

    @FXML
    public TextArea point;

    public AnchorPane paneDepart;

    private int nbJoueurs;

    List<Label> list=new ArrayList<Label>();
    List<TextField> listNom =new ArrayList<TextField>();



    public  Label j1=new Label();
    public  Label j2=new Label();
    public  Label j3=new Label();
    public  Label j4=new Label();
    public TextField nomJ1=new TextField();
    public TextField nomJ2=new TextField();
    public TextField nomJ3=new TextField();
    public TextField nomJ4=new TextField();




    public FXMLLoader ldr = new FXMLLoader(getClass().getResource("ChoixNbJoueurs.fxml"));


    public Joueur joueur1 = new Joueur();
    public Joueur joueur2 = new Joueur();
    public Joueur joueur3 = new Joueur();
    public Joueur joueur4 = new Joueur();
    private Image etoileR = new Image("pions/etoileRouge.png");
    private Image etoileV = new Image("pions/etoileViolette.png");
    private Image etoileO = new Image("pions/etoileOrange.png");
    private Image pionManche = new Image("pions/pionManche.png");
    private Image ouvrierV = new Image("pions/ouvrierViolet.png");
    private Image ouvrierO = new Image("pions/ouvrierOrange.png");
    private Image ouvrierR = new Image("pions/ouvrierRouge.png");
    private Image tuile1 = new Image("pions/tuile1.png");
    private Image tuile2 = new Image("pions/tuile2.png");
    private Image tuile3 = new Image("pions/tuile3.png");
    private Image tuile4 = new Image("pions/tuile4.png");
    private Image tuile5 = new Image("pions/tuile5.png");
    private Image tuile6 = new Image("pions/tuile6.png");
    private Image tuile7 = new Image("pions/tuile7.png");
    private Image tuile8 = new Image("pions/tuile8.png");
    private Image tuile9 = new Image("pions/tuile9.png");
    private Image tuile10 = new Image("pions/tuile10.png");
    private Image tuile11 = new Image("pions/tuile11.png");
   public  List<ImageView> listebat=new ArrayList<ImageView>();

    public ImageView bat1=new ImageView(tuile1);
    public ImageView bat2=new ImageView(tuile2);
    public ImageView bat3=new ImageView(tuile3);
    public ImageView bat4=new ImageView(tuile4);
    public  ImageView bat5=new ImageView(tuile5);
    public ImageView bat6=new ImageView(tuile6);
    public ImageView bat7=new ImageView(tuile7);
    public ImageView bat8=new ImageView(tuile8);
    public ImageView bat9=new ImageView(tuile9);
    public ImageView bat10=new ImageView(tuile10);
    public ImageView bat11=new ImageView(tuile11);
    public ImageView depart=new ImageView();
    public ImageView departJ2=new ImageView();



    // Définition des couleurs
    private String rouge = "rouge";
    private String violet = "violet";
    private String orange ="orange";
    public App() throws IOException {


        ldr.setRoot(this);
        ldr.setController(this);
        ldr.load();
    }

    public void handle(ActionEvent event){
        Button btn = (Button) event.getSource();
        String id = btn.getId();
        System.out.println(id);
        setEtoileJ(etoileR);

        btn.setMaxSize(74,71);

        btn.setGraphic(new ImageView(etoileR));
    }

    public void handle2(ActionEvent event){
        Button btn = (Button) event.getSource();
        String id = btn.getId();
        System.out.println(id);
        btn.setGraphic(new ImageView(etoileV));
    }

    public void changeJoueurs(ActionEvent event) throws IOException {
        if (changeJoueur.getText().equals("Change to "+joueur2.getNom())){
            changeJoueur.setText("Change to "+joueur1.getNom());
            File f=new File("src/sample/couleurs2.txt");
            setPions(joueur2.getCouleur());
            objectifJ1.setText(joueur2.getListeObjectif().get(0).getObjectif());
            pointObjectif.setText(String.valueOf((joueur2.getListeObjectif().get(0).getNbpoint())));
            if (pointsHaut.getChildren().get(0)==depart){
                if (joueur2.getCouleur().equals(rouge)) {
                    departJ2.setImage(etoileR);
                }else if (joueur2.getCouleur().equals(violet)){
                    departJ2.setImage(etoileV);
                }else if (joueur2.getCouleur().equals(orange)){
                    departJ2.setImage(etoileO);
                }
            }
            if (joueur2.getPionDepart()==joueur1.getPionDepart()){
                departJ2.setOpacity(1);
                depart.setOpacity(0);
            }else {
                departJ2.setOpacity(1);
                depart.setOpacity(1);
            }


        } else if (changeJoueur.getText().equals("Change to "+joueur1.getNom())){
            changeJoueur.setText("Change to "+joueur2.getNom());
            File f=new File("src/sample/couleurs.txt");
            setPions(joueur1.getCouleur());
            objectifJ1.setText(joueur1.getListeObjectif().get(0).getObjectif());
            pointObjectif.setText(String.valueOf((joueur1.getListeObjectif().get(0).getNbpoint())));
            if (pointsHaut.getChildren().get(0)==depart){
                if (joueur1.getCouleur().equals(rouge)) {
                    depart.setImage(etoileR);
                }else if (joueur1.getCouleur().equals(violet)){
                    depart.setImage(etoileV);
                }else if (joueur1.getCouleur().equals(orange)){
                    depart.setImage(etoileO);
                }
            }
            if (joueur2.getPionDepart()==joueur1.getPionDepart()){
                departJ2.setOpacity(0);
                depart.setOpacity(1);
            }else {
                departJ2.setOpacity(1);
                depart.setOpacity(1);
            }
        }
    }

    public void choose (ActionEvent event) throws IOException {
        Button btn = (Button) event.getSource();
        String id = btn.getId();

        this.ldr = new FXMLLoader(getClass().getResource("Choose2.fxml"));
        ldr.setRoot(this);
        ldr.setController(this);





        switch (id){
            case "J1R":
                joueur1.setCouleur(rouge);
                this.getChildren().clear();
                ldr.load();

                break;
            case "J1V":
                joueur1.setCouleur(violet);

                this.getChildren().clear();
                ldr.load();
                break;
            case "J1O":
                joueur1.setCouleur(orange);
                this.getChildren().clear();
                ldr.load();

                break;
        }
    }

    public void choose2 (ActionEvent event) throws IOException {
        Button btn = (Button) event.getSource();
        String id = btn.getId();
        this.ldr = new FXMLLoader(getClass().getResource("Menu.fxml"));
        ldr.setRoot(this);
        ldr.setController(this);




        switch (id){
            case "J2R":
                if (joueur1.getCouleur()!=rouge){
                    joueur2.setCouleur(rouge);
                    this.getChildren().clear();
                    ldr.load();
                }

                break;
            case "J2V":
                if (joueur1.getCouleur()!=violet){
                    joueur2.setCouleur(violet);
                    this.getChildren().clear();
                    ldr.load();
                }


                break;
            case "J2O":
                if (joueur1.getCouleur()!=orange){
                    joueur2.setCouleur(orange);
                    this.getChildren().clear();
                    ldr.load();
                }
        }
    }

    public boolean verifCouleur(String couleur) {
        File f = new File("src/sample/couleurs.txt");
        try {
            BufferedReader reader = new BufferedReader(new FileReader(f));
            String line = reader.readLine();
            return line.equals(couleur);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            System.out.println("Fichier manquant");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @FXML
    public void LoadSecond() throws Exception
    {
        Pane fxmlLoader = FXMLLoader.load(getClass().getResource("Plateau2.fxml"));
        pane1.getChildren().setAll(fxmlLoader);
        System.out.println("J'ai changé de scene");
    }

    @FXML
    public void LoadOne() throws Exception
    {
        Pane fxmlLoader = FXMLLoader.load(getClass().getResource("sample.fxml"));
        pane2.getChildren().setAll(fxmlLoader);
        System.out.println("J'ai changé de scene");
    }

    @FXML
    public void Commencer() throws Exception
    {


        this.getChildren().clear();
        this.ldr = new FXMLLoader(getClass().getResource("sample.fxml"));
        ldr.setRoot(this);
        ldr.setController(this);
        ldr.load();

        System.out.println("J'ai changé de scene");
    }

    @FXML
    public void setEtoileJ(Image i){
        etoileJ.setImage(i);
    }

    @FXML
    public void changeTour(){
        joueur1.setCouleur("rouge");
        System.out.println(joueur1.getCouleur());
        if(tour1.getImage()== pionManche) {
            tour2.setImage(pionManche);
            tour1.setImage(null);
        }
        else if(tour2.getImage()== pionManche) {
            tour3.setImage(pionManche);
            tour2.setImage(null);
        }
        else if(tour3.getImage()== pionManche) {
            tour4.setImage(pionManche);
            tour3.setImage(null);
        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        if (tour1 != null) {
            tour1.setImage(pionManche);
            List<Objectif> objectifs = chargeObectif(); //potentiellement modifiable pour ne donner que des objectifs jouables
            Objectif o1 = objectifs.get((int) (Math.random() * 4));
            Objectif o2 = objectifs.get((int) (Math.random() * 4));
            joueur1.addObjectif(o1);
            objectifs.remove(o1);
            joueur2.addObjectif(o2);
            objectifs.remove(o2);
            objectifJ1.setText(joueur1.getListeObjectif().get(0).getObjectif());
            pointObjectif.setText(String.valueOf(joueur1.getListeObjectif().get(0).getNbpoint()));

            if (joueur1.getCouleur().equals(rouge)) {
                depart.setImage(etoileR);
            }else if (joueur1.getCouleur().equals(violet)){
                depart.setImage(etoileV);
            }else if (joueur1.getCouleur().equals(orange)){
                depart.setImage(etoileO);
            }
            if (joueur2.getCouleur().equals(rouge)) {
                departJ2.setImage(etoileR);
            }else if (joueur2.getCouleur().equals(violet)){
                departJ2.setImage(etoileV);
            }else if (joueur2.getCouleur().equals(orange)){
                departJ2.setImage(etoileO);
            }
            depart.setFitWidth(30);
            depart.setFitHeight(30);
            departJ2.setFitWidth(30);
            departJ2.setFitHeight(30);
            pointsHaut.getChildren().set(0,depart);
            pointsHaut.add(departJ2,0,0);
            departJ2.setOpacity(0);
            joueur1.setPionDepart(0);
            joueur2.setPionDepart(0);


        }
        if (ouvrier != null && etoileJ != null) {
            try {
                setPions(joueur1.getCouleur());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (changeJoueur!=null){
            changeJoueur.setText("Change to "+joueur2.getNom());
        }
        if (marche30!=null){
            listebat.add(bat1);
            listebat.add(bat2);
            listebat.add(bat3);
            listebat.add(bat4);
            listebat.add(bat5);
            listebat.add(bat6);
            listebat.add(bat7);
            listebat.add(bat8);
            listebat.add(bat9);
            listebat.add(bat10);
            listebat.add(bat11);



            for (ImageView i:listebat
                 ) {
                i.setFitHeight(60);
                i.setFitWidth(60);

            }
            marche30.setGraphic(listebat.get(0));
            marche40.setGraphic(listebat.get(1));
            marche50.setGraphic(listebat.get(2));
            marche60.setGraphic(listebat.get(3));
            marche70.setGraphic(listebat.get(4));
            marche80.setGraphic(listebat.get(5));
            marche31.setGraphic(listebat.get(6));
            marche41.setGraphic(listebat.get(7));
            marche51.setGraphic(listebat.get(8));
            marche61.setGraphic(listebat.get(9));
            marche71.setGraphic(listebat.get(10));


        }
    }

    public static List<Objectif> chargeObectif(){
        Objectif objectif1=new Objectif("objectif1");
        objectif1.setNbpoint(3);
        Objectif objectif2=new Objectif("objectif2");
        objectif2.setNbpoint(2);
        Objectif objectif3=new Objectif("objectif3");
        objectif3.setNbpoint(4);
        Objectif objectif4=new Objectif("objectif4");
        objectif4.setNbpoint(1);

        List<Objectif> list=new ArrayList<Objectif>();
        list.add(objectif1);
        list.add(objectif2);
        list.add(objectif3);
        list.add(objectif4);
        return list;
    }

    public void setPions(String couleur) throws IOException {

        if (couleur.equals("rouge")){
            ouvrier.setImage(ouvrierR);
            etoileJ.setImage(etoileR);
        }else if (couleur.equals("violet")){
            ouvrier.setImage(ouvrierV);
            etoileJ.setImage(etoileV);
        }else if (couleur.equals("orange")){
            ouvrier.setImage(ouvrierO);
            etoileJ.setImage(etoileO);
        }
    }
    @FXML
    public void gagnerPoint() throws IOException {
        if (changeJoueur.getText().equals("Change to "+joueur2.getNom())){
            setDepart(Integer.parseInt(point.getText()),joueur1);
        }else if (changeJoueur.getText().equals("Change to "+joueur1.getNom())){
            setDepart2(Integer.parseInt(point.getText()),joueur2);
        }




    }
    public void setDepart(int position , Joueur j) throws IOException {

        int tampon=j.getPionDepart()+position;

        if (tampon>0&&tampon<=23){
            pointsHaut.getChildren().remove(depart);
            pointsHaut.add(depart,tampon,0);

            j.setPionDepart(tampon);
        }else if (tampon>23&&tampon<=28){
            pointsDroit.getChildren().remove(depart);
            pointsDroit.add(depart,0,tampon-24);
        }else if (tampon>28&&tampon<53){
            pointsBas.getChildren().remove(depart);
            pointsBas.add(depart,52-tampon,0);
        }
        else if (tampon==53) {
            pointsBas.getChildren().remove(depart);
            pointsGauche.add(depart,0,58-tampon);
        }else if (tampon>53&&tampon<=57){
            pointsGauche.getChildren().remove(depart);
            pointsGauche.add(depart,0,57-tampon);
        }
        if (tampon>57){
            this.getChildren().clear();
            this.ldr = new FXMLLoader(getClass().getResource("Victoire.fxml"));
            ldr.setRoot(this);
            ldr.setController(this);
            ldr.load();
            victoire.setText(j.getNom());

        }
        j.setPionDepart(tampon);

    }
    public void setDepart2(int position , Joueur j) throws IOException {
        pointsHaut.getChildren().remove(departJ2);

        int tampon=j.getPionDepart()+position;
        if (tampon>0&&tampon<=23){
            pointsHaut.getChildren().remove(departJ2);
            pointsHaut.add(departJ2,tampon,0);

            j.setPionDepart(tampon);
        }else if (tampon>23&&tampon<=28){
            pointsDroit.getChildren().remove(departJ2);
            pointsDroit.add(departJ2,0,tampon-24);
        }else if (tampon>28&&tampon<53){
            pointsBas.getChildren().remove(departJ2);
            pointsBas.add(departJ2,52-tampon,0);

        }
        else if (tampon==53) {
            pointsBas.getChildren().remove(departJ2);
            pointsGauche.add(departJ2,0,58-tampon);
        }else if (tampon>53&&tampon<=58){
            pointsGauche.getChildren().remove(departJ2);
            pointsGauche.add(departJ2,0,57-tampon);
        }
        if (tampon>58){
            this.getChildren().clear();

            this.ldr = new FXMLLoader(getClass().getResource("Victoire.fxml"));
            ldr.setRoot(this);
            ldr.setController(this);
            ldr.load();
            victoire.setText(j.getNom());

        }
        j.setPionDepart(tampon);
    }
    public void validNb() throws IOException {

        List<Label> list=new ArrayList<Label>();
        List<TextField> listText=new ArrayList<TextField>();
        listText=setText();

        if (valid.getText().equals("Commencer")){

            joueur1.setNom(this.listNom.get(0).getText());
            joueur2.setNom(this.listNom.get(1).getText());
            if (this.listNom.size()==3){
                joueur2.setNom(this.listNom.get(2).getText());
            }
            if (this.listNom.size()==4){
                joueur2.setNom(this.listNom.get(3).getText());
            }


            this.getChildren().clear();
            this.ldr = new FXMLLoader(getClass().getResource("Choose.fxml"));
            ldr.setRoot(this);
            ldr.setController(this);
            ldr.load();
        }
        if (check2.isSelected()){
            list=setLabel(2);
            pane1.getChildren().removeAll(check2,check3,check4,lab1);

            for (int i=0;i<2;i++){
                list.get(i).setId("label"+i);
                list.get(i).setLayoutX(150*i+10);
                list.get(i).setLayoutY(50);
                listText.get(i).setLayoutX(150*i+10);
                listText.get(i).setLayoutY(100);
                this.listNom.add(listText.get(i));

                pane1.getChildren().addAll(list.get(i));
                pane1.getChildren().addAll(listText.get(i));
                valid.setText("Commencer");

            }
            this.nbJoueurs=2;
            this.list=list;
        }else if(check3.isSelected()){
            list=setLabel(3);
            pane1.getChildren().removeAll(check2,check3,check4,lab1);
            for (int i=0;i<3;i++){
                list.get(i).setLayoutX(150*i+10);
                list.get(i).setLayoutY(50);
                listText.get(i).setLayoutX(150*i+10);
                listText.get(i).setLayoutY(100);
                this.listNom.add(listText.get(i));

                pane1.getChildren().addAll(list.get(i));
                pane1.getChildren().addAll(listText.get(i));
                valid.setText("Commencer");
            }
            this.nbJoueurs=3;
            this.list=list;
        }else if(check4.isSelected()){
            list=setLabel(4);
            pane1.getChildren().removeAll(check2,check3,check4,lab1);
            for (int i=0;i<4;i++){
                list.get(i).setLayoutX(150*i+10);
                list.get(i).setLayoutY(50);
                listText.get(i).setLayoutX(150*i+10);
                listText.get(i).setLayoutY(100);
                this.listNom.add(listText.get(i));

                pane1.getChildren().addAll(list.get(i));
                pane1.getChildren().addAll(listText.get(i));
                valid.setText("Commencer");
            }
            this.list=list;
            this.nbJoueurs=4;
        }


    }

    public List<Label> setLabel(int nb){
        List<Label> list=new ArrayList<Label>();
        list.add(j1);
        list.add(j2);
        list.add(j3);
        list.add(j4);
        for(int i=0 ;i<nb;i++){
            int truc=i+1;
            list.get(i).setText("Joueur "+truc+" entre son nom");
        }
        return list;
    }

    public List<TextField> setText(){
        List<TextField> list =new ArrayList<TextField>();
        list.add(nomJ1);
        list.add(nomJ2);
        list.add(nomJ3);
        list.add(nomJ4);
        return  list;

    }
}
