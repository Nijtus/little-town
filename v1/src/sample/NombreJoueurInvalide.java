package sample;

class NombreJoueursInvalide extends Exception{
  public NombreJoueursInvalide(){
    super("Vous essayez d'instancier une classe Ville avec un nombre d'habitants négatif !");
  }
}
