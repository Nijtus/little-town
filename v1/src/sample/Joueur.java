package sample;

import java.util.ArrayList;
import java.util.List;

public class Joueur {

    private String nom;
    private List<PionBatiment> listeBatiment;
    private List<PionOuvrier> listeOuvrier;
    private List<Objectif> listeObjectifs;
    private boolean isPremierJoueur;
    private String couleur;
    private int pionDepart;



    public Joueur(){
      listeBatiment = new ArrayList<>();
      listeOuvrier= new ArrayList<>();
      listeObjectifs= new ArrayList<>();

    }
    public Joueur(String nom){
        this.nom=nom;
        listeBatiment = new ArrayList<>();
        listeOuvrier= new ArrayList<>();
        listeObjectifs= new ArrayList<>();
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return super.toString();
    }

    public boolean isPremierJoueur() {
        return isPremierJoueur;
    }

    public void setListeBatiment(List<PionBatiment> listeBatiment) {
        this.listeBatiment = listeBatiment;
    }

    public List<PionBatiment> getListeBatiment() {
        return listeBatiment;
    }

    public List<PionOuvrier> getListeOuvrier() {
        return listeOuvrier;
    }

    public List<Objectif> getListeObjectif(){
        return listeObjectifs;
    }

    public void setListeOuvrier(List<PionOuvrier> listeOuvrier) {
        this.listeOuvrier = listeOuvrier;
    }

    public void setPremierJoueur(boolean premierJoueur) {
        isPremierJoueur = premierJoueur;
    }

    public void addObjectif(Objectif o){
      listeObjectifs.add(o);
    }

    public int getPionDepart() {
        return pionDepart;
    }

    public void setPionDepart(int pionDepart) {
        this.pionDepart = pionDepart;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public boolean choisirJoueur() {
        double rand = Math.random();
        if(rand < 0.5) {
            return true;
        } return false;
    }



}

