package sample;

public class CarteObjectif {
    private int nombrePoints;
    private String description;

    public CarteObjectif(int nombrePoints, String description) {
        this.nombrePoints = nombrePoints;
        this.description = description;
    }
}
